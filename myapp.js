const manifestUri = [
    'https://signage-api.itotem.ro/dash/f85e34d980684d70939b0e6738e67ad0.mp4/manifest.mpd',
    'https://signage-api.itotem.ro/dash/a18b133635714d6499e34ea06b2095f5.mp4/manifest.mpd'
];

let current_playlist = []

function initApp() {
  // Install built-in polyfills to patch browser incompatibilities.
  shaka.polyfill.installAll();

  // Check to see if the browser supports the basic APIs Shaka needs.
  if (shaka.Player.isBrowserSupported()) {
    // Everything looks good!
    initPlayer();
  } else {
    // This browser does not have the minimum set of APIs we need.
    console.error('Browser not supported!');
  }
}

function get_video_to_play(){
  if (current_playlist.length === 0 ){
    // const url_string = window.location.href;
    // const url = new URL(url_string);
    // const video_uid = url.searchParams.get("uid");
  
    // current_playlist.push(video_uid); 
    current_playlist=['f85e34d980684d70939b0e6738e67ad0', 'a18b133635714d6499e34ea06b2095f5'] 
  }

  const next_video_uid = current_playlist.shift();

  return next_video_uid;
}

function initPlayer() {
  // Create a Player instance.
  const video1 = document.getElementById('video1');
  const player = new shaka.Player(video1);
  video1.muted = true;

  // Attach player to the window to make it easy to access in the JS console.
  window.player = player;
  window.video = video1;
  // Listen for error events.
  player.addEventListener('error', onErrorEvent);
  video.addEventListener('ended', handleVideoEnded);
  _play();
}

function handleVideoEnded(){
  console.log('play again');
  sendMessage({uid: current_playlist[0]})
  _play();
}


async function _play(){
  // Try to load a manifest.
  // This is an asynchronous process
  try {
    const video_uid = get_video_to_play();

    while (window.player.firstChild){
      CONSTANT.debug && console.log('remove current video src');
      window.player.removeChild(player.firstChild);
    }

    // await window.player.load(`https://signage-api.itotem.ro/dash/${video_uid}.mp4/manifest.mpd`);
    await window.player.load(`https://signage-api.itotem.ro/static/${video_uid}.mp4`);
    // This runs if the asynchronous load is successful.
    console.log('The video has now been loaded!');
    window.video.play();
  } catch (e) {
    // onError is executed if the asynchronous load fails.
    onError(e);
  }
}

function onErrorEvent(event) {
  // Extract the shaka.util.Error object from the event.
  onError(event.detail);
}

function onError(error) {
  // Log the error.
  console.error('Error code', error.code, 'object', error);
}

document.addEventListener('DOMContentLoaded', initApp);


sendMessage = (payload) => {
  const message = JSON.stringify({
    prefix: 'video_ended',
    payload: payload
  });

  if(window.hasOwnProperty('ReactNativeWebView')){
    window.ReactNativeWebView.postMessage(message);
  }

  if (document.hasOwnProperty('postMessage')) {
    document.postMessage(message, '*');
  } else if (window.hasOwnProperty('postMessage')) {
    window.postMessage(message, '*');
  } else {
    console.log('unable to find postMessage');
  }
};